clc; close all; clear all;

% Add the blendModes functionality
addpath('../blendModes')

% Create a cascade detector object.
faceDetector = vision.CascadeObjectDetector();

video_file = input('Please enter the relative video path.\n' ,'s');
hat_file = input('Please enter the relative hat path.\n', 's');

% Video reader for images that will be rendered into final output
imgVideo = VideoReader(video_file);
numFrames = imgVideo.NumberOfFrames;
vidWidth = imgVideo.Width;
vidHeight = imgVideo.Height;


% Read a video frame and run the face detector.
videoFileReader = vision.VideoFileReader(video_file);
videoFrame      = step(videoFileReader);
bbox            = step(faceDetector, videoFrame);

% Convert the first box to a polygon.
% This is needed to be able to visualize the rotation of the object.
x = bbox(1, 1); y = bbox(1, 2); w = bbox(1, 3); h = bbox(1, 4);
bboxPolygon = [x, y, x+w, y, x+w, y+h, x, y+h];

% Draw the returned bounding box around the detected face.
videoFrame = insertShape(videoFrame, 'Polygon', bboxPolygon);

% Detect feature points in the face region.
points = detectMinEigenFeatures(rgb2gray(videoFrame), 'ROI', bbox);

% Create a point tracker and enable the bidirectional error constraint to
% make it more robust in the presence of noise and clutter.
pointTracker = vision.PointTracker('MaxBidirectionalError', 2);

% Initialize the tracker with the initial point locations and the initial
% video frame.
points = points.Location;
initialize(pointTracker, points, videoFrame);

% videoPlayer  = vision.VideoPlayer('Position',...
%     [100 100 [size(videoFrame, 2), size(videoFrame, 1)]+30]);

% Create the movie struct
mov(1:numFrames) = ...
    struct('cdata',zeros(vidHeight,vidWidth,3,'uint8'),...
           'colormap',[]);

% Make a copy of the points to be used for computing the geometric
% transformation between the points in the previous and the current frames
oldPoints = points;

initHat = imread(hat_file);
[h, w, d] = size(initHat);
percentage = bbox(1,3) / w;

hat = imresize(initHat, percentage);

for i = 1 : numFrames
    % get the next frame
    videoFrame = step(videoFileReader);

    % Track the points. Note that some points may be lost.
    [points, isFound] = step(pointTracker, videoFrame);
    visiblePoints = points(isFound, :);
    oldInliers = oldPoints(isFound, :);

    if size(visiblePoints, 1) >= 2 % need at least 2 points

        % Estimate the geometric transformation between the old points
        % and the new points and eliminate outliers
        [xform, oldInliers, visiblePoints] = estimateGeometricTransform(...
            oldInliers, visiblePoints, 'similarity', 'MaxDistance', 4);

        % Apply the transformation to the bounding box
        [bboxPolygon(1:2:end), bboxPolygon(2:2:end)] ...
            = transformPointsForward(xform, bboxPolygon(1:2:end), bboxPolygon(2:2:end));

        % Reset the points
        oldPoints = visiblePoints;
        setPoints(pointTracker, oldPoints);
    end
    b = bboxPolygon(6);
    a = bboxPolygon(1);
    c = bboxPolygon(5);
    d = bboxPolygon(8);
    h = sqrt((a-c)^2 + (b-d)^2);
    theta = asin((d-b)/h);
    theta = radtodeg(theta);

    [h, w, d] = size(initHat);
    head_width = bboxPolygon(3) - bboxPolygon(1);
    percentage = head_width / w;
    hat = imresize(initHat, percentage);
    hat = imrotate(hat, theta);

    initFrame = read(imgVideo,i);
%    videoFrame = insertShape(initFrame, 'Polygon', bboxPolygon);
%     videoFrame = insertMarker(videoFrame, visiblePoints, '+', ...
%             'Color', 'white');

    [h, w, d] = size(hat);
    x = bboxPolygon(1);
    y = bboxPolygon(2) - h;
    videoFrame = blendMode(videoFrame, hat, 'Normal', x, y + 10);
    
    videoFrame = im2uint8(videoFrame);

    mov(i).cdata = videoFrame;

end

f = figure;
set(f, 'position', [150 150 vidWidth vidHeight])
movie(mov,1,imgVideo.FrameRate);

% Clean up
release(videoFileReader);
release(videoPlayer);
release(pointTracker);